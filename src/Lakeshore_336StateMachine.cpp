static const char *RcsId = "$Id $";
//+=============================================================================
//
// file :         Lakeshore_336StateMachine.cpp
//
// description :  C++ source for the Lakeshore_336 and its alowed 
//                methods for commands and attributes
//
// project :      TANGO Device Server
//
// $Author: olivierroux $
//
// $Revision: 1.1 $
// $Date: 2010-11-05 10:36:03 $
//
// SVN only:
// $HeadURL: $
//
// CVS only:
// $Source: /users/chaize/newsvn/cvsroot/Instrumentation/Lakeshore_336/src/Lakeshore_336StateMachine.cpp,v $
// $Log: not supported by cvs2svn $
//
// copyleft :     European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                FRANCE
//
//-=============================================================================
//
//  		This file is generated by POGO
//	(Program Obviously used to Generate tango Object)
//
//         (c) - Software Engineering Group - ESRF
//=============================================================================

#include <tango.h>
#include <Lakeshore_336.h>
#include <Lakeshore_336Class.h>

/*====================================================================
 *	This file contains the methods to allow commands and attributes
 * read or write execution.
 *
 * If you wand to add your own code, add it between 
 * the "End/Re-Start of Generated Code" comments.
 *
 * If you want, you can also add your own methods.
 *====================================================================
 */

namespace Lakeshore_336_ns
{

//=================================================
//		Attributes Allowed Methods
//=================================================

//+----------------------------------------------------------------------------
//
// method : 		Lakeshore_336::is_temperature_allowed
// 
// description : 	Read/Write allowed for temperature attribute.
//
//-----------------------------------------------------------------------------
bool Lakeshore_336::is_temperature_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		Lakeshore_336::is_output_allowed
// 
// description : 	Read/Write allowed for output attribute.
//
//-----------------------------------------------------------------------------
bool Lakeshore_336::is_output_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		Lakeshore_336::is_range_allowed
// 
// description : 	Read/Write allowed for range attribute.
//
//-----------------------------------------------------------------------------
bool Lakeshore_336::is_range_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		Lakeshore_336::is_deadBand_allowed
// 
// description : 	Read/Write allowed for deadBand attribute.
//
//-----------------------------------------------------------------------------
bool Lakeshore_336::is_deadBand_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		Lakeshore_336::is_inputA_allowed
// 
// description : 	Read/Write allowed for inputA attribute.
//
//-----------------------------------------------------------------------------
bool Lakeshore_336::is_inputA_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		Lakeshore_336::is_inputB_allowed
// 
// description : 	Read/Write allowed for inputB attribute.
//
//-----------------------------------------------------------------------------
bool Lakeshore_336::is_inputB_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		Lakeshore_336::is_inputC_allowed
// 
// description : 	Read/Write allowed for inputC attribute.
//
//-----------------------------------------------------------------------------
bool Lakeshore_336::is_inputC_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		Lakeshore_336::is_inputD_allowed
// 
// description : 	Read/Write allowed for inputD attribute.
//
//-----------------------------------------------------------------------------
bool Lakeshore_336::is_inputD_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}

//=================================================
//		Commands Allowed Methods
//=================================================

//+----------------------------------------------------------------------------
//
// method : 		Lakeshore_336::is_Off_allowed
// 
// description : 	Execution allowed for Off command.
//
//-----------------------------------------------------------------------------
bool Lakeshore_336::is_Off_allowed(const CORBA::Any &any)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		Lakeshore_336::is_Low_allowed
// 
// description : 	Execution allowed for Low command.
//
//-----------------------------------------------------------------------------
bool Lakeshore_336::is_Low_allowed(const CORBA::Any &any)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		Lakeshore_336::is_Medium_allowed
// 
// description : 	Execution allowed for Medium command.
//
//-----------------------------------------------------------------------------
bool Lakeshore_336::is_Medium_allowed(const CORBA::Any &any)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		Lakeshore_336::is_High_allowed
// 
// description : 	Execution allowed for High command.
//
//-----------------------------------------------------------------------------
bool Lakeshore_336::is_High_allowed(const CORBA::Any &any)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}

//+----------------------------------------------------------------------------
//
// method : 		Lakeshore_336::is_LoopSelectInput_allowed
// 
// description : 	Execution allowed for LoopSelectInput command.
//
//-----------------------------------------------------------------------------
bool Lakeshore_336::is_LoopSelectInput_allowed(const CORBA::Any &any)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		Lakeshore_336::is_IORaw_allowed
// 
// description : 	Execution allowed for IORaw command.
//
//-----------------------------------------------------------------------------
bool Lakeshore_336::is_IORaw_allowed(const CORBA::Any &any)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		Lakeshore_336::is_Stop_allowed
// 
// description : 	Execution allowed for Stop command.
//
//-----------------------------------------------------------------------------
bool Lakeshore_336::is_Stop_allowed(const CORBA::Any &any)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}


}	// namespace Lakeshore_336_ns
