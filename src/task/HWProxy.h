
//- Project : Sample Cryo Environement for DEIMOS
//- driven by a Lakeshore 336
//- file : HWProxy.h
//- threaded reading of the HW


#ifndef __HW_PROXY_H__
#define __HW_PROXY_H__

#include <tango.h>
#include <yat4tango/DeviceTask.h>
#include <yat/time/Timer.h>
#include <yat4tango/ThreadSafeDeviceProxy.h>

#include "RampingStateManager.h"

namespace HWProxy_ns
{
  // ============================================================================
  // some defines enums and constants
  // ============================================================================
  //- startup and communication errors
  const size_t MAX_COM_RETRIES = 20;
  const size_t MAX_COMMAND_RETRIES = 5;

  typedef enum
  {
    HWP_UNKNOWN_ERROR        = 0,
    HWP_INITIALIZATION_ERROR,
    HWP_COMMUNICATION_ERROR,
    HWP_COMMAND_ERROR,
    HWP_HARDWARE_ERROR,
    HWP_SOFTWARE_ERROR,
    HWP_NO_ERROR
  } ComState;  



  //- the status strings for ComState
  const size_t HWP_STATE_MAX_SIZE = 7;
  static const std::string hw_state_str[HWP_STATE_MAX_SIZE] =
  {
    "Unknown Error",
    "Initialisation Error",
    "Communication Error",
    "Command Error",
    "Hardware Error",
    "Software Error",
    "Communication Running"
  };

  // ============================================================================
  // some structs for messages and configuration
  // ============================================================================

  typedef struct LowLevelMsg
  {
    std::string cmd;
  }LowLevelMsg;


  // ========================================================
  //---------------------------------------------------------  
  //- the YAT user messages 
  const size_t EXEC_LOW_LEVEL_MSG         = yat::FIRST_USER_MSG + 1000;
  const size_t SET_DEADBAND_MSG           = yat::FIRST_USER_MSG + 1007;
  const size_t SET_SETPOINT_MSG           = yat::FIRST_USER_MSG + 1009;
  const size_t STOP_MSG                   = yat::FIRST_USER_MSG + 1010;
  const size_t LOOP_SELECT_INPUT_MSG      = yat::FIRST_USER_MSG + 1011;
  const size_t LOOP_SELECT_RANGE_MSG      = yat::FIRST_USER_MSG + 1012;


  //------------------------------------------------------------------------
  //- HWProxy Class
  //- read the HW 
  //------------------------------------------------------------------------
  class HWProxy : public yat4tango::DeviceTask
  {
    public :

  //- the configuration structure
  typedef struct Config
  {
    //- members
    std::string com_device_proxy;
    size_t startup_timeout_ms;
    size_t read_timeout_ms;
    size_t periodic_timeout_ms;
    //- for the ramping state manager
    time_t time_in_deadband;

    //- the loop number
    size_t loop_number;
    


    //- Ctor -------------
    Config ();
    //- Ctor -------------
    Config ( const Config & _src);
    //- Ctor -------------
    Config (std::string url );

    //- operator = -------
    void operator = (const Config & src);
  }Config;
   
    //- Constructeur/destructeur
    HWProxy (Tango::DeviceImpl * _host_device,
             Config & conf);

    virtual ~HWProxy ();

    //- the number of communication successfully done
    unsigned long get_com_error (void)   { return com_error; };
    //- the number of communication errors
    unsigned long get_com_success (void) { return com_success; };
    //- the number of commands successfully done
    unsigned long get_command_error (void)   { return command_error; };
    //- the number of command errors
    unsigned long get_command_success (void) { return command_ok; };
    //- the state and status of communication
    ComState get_com_state (void)        {return com_state; };
    std::string get_com_status (void)    { return hw_state_str [com_state]; };
    std::string get_last_error (void)    { return last_error; };


    //- sends a Low Level Command (do not check the syntax)  to the hardware return the response -blocks the com!
    std::string exec_low_level_command (std::string cmd);

    //- returns the Lakeshore state value;
    Tango::DevState get_lakeshore_state (void);

    //- return the status as string
    std::string get_lakeshore_status (void);

    //- access to temperature 1 to 4
    double get_input (size_t channel) throw (Tango::DevFailed)
    {
      if (channel > 0 && channel < 5)
        return temperature_read [channel -1];
      else
      {
        Tango::Except::throw_exception ("DATA_OUT_OF_RANGE",
                                        " temperature index must be in [1...4]",
                                         "HWProxy::get_input");
      }
    };

    //- access temperature (the input configured for this loop)
    double get_temperature (void) throw (Tango::DevFailed)
    {
      if (this->com_state == HWP_INITIALIZATION_ERROR)
      {
        Tango::Except::throw_exception ("DATA_OUT_OF_RANGE",
                                        " temperature index must be in [0...3]",
                                         "HWProxy::get_temperature");
      }
      return temperature_read [this->input_number - 1];
    };

    //- and  get the setpoint 
    double setpoint (void)
    {
      return setpoint_read;
    };

    size_t get_range (void)
    {
      return range;
    };

    size_t get_mode (void)
    {
      return mode;
    };

    size_t get_input (void)
    {
      return input_number;
    };

    size_t get_loop (void)
    {
      return conf.loop_number;
    };
    size_t is_powerup_enabled (void)
    {
      return power_up_enable;
    };

    double get_output (void)
    {
      return output_read;
    }


    //- Acces to deadband ramp utility
    double deadband (void) { return reg_manager->get_deadband (); };

    //- Access to State of the Ramp utility
    Tango::DevState check_ramp_state (void) { return reg_manager->get_state (); };

    //- Access to the Ramp Status 
    std::string check_ramp_status (void) { return reg_manager->get_status (); };


  protected:
	  //- process_message (implements yat4tango::DeviceTask pure virtual method)
	  virtual void process_message (yat::Message& msg)throw (Tango::DevFailed);

  private :

    //- get values on the hard : 
    //- the 4 temperatures KRDG
    bool read_temperatures (void);

    //- the setpoint SETP
    bool read_setpoint (void);

    //- the output percent HTR
    bool read_output (void);

    //- the input status RDGST OPSTR HTSTR
    bool read_lakeshore_state (void);

    //- get the loop configuration (the input associated)
    bool read_outmode (void);

    //- get the range
    bool read_range (void);

 
    //-----------------------communication stuff ---------------------------

    //- get a value on the hardware
    bool write_read (std::string cmd, std::string & response);

    //- write command (no answer) on the hardware
    bool write (std::string cmd);

    //- the ThreadSafeDeviceProxy on (Ethernet | GPIB)
    yat4tango::ThreadSafeDeviceProxy * comproxy;

    //- au cas ou....
    yat::Mutex lock;

    //- the configuration
    Config conf;

    //- the host device 
    Tango::DeviceImpl * host_dev;

    //- the periodic execution time
    size_t periodic_exec_ms;
 
    //- the state and status stuff
    ComState com_state;
    std::string com_status;

    std::string last_error;

    //- for Com error attribute
    unsigned long com_error;

    //- for Com OK attribute
    unsigned long com_success;

    //- internal error counter
    unsigned long consecutive_com_errors;

    //- for command error (command refused by Lakeshore)
    unsigned long command_ok;
    unsigned long command_error;
    unsigned long consecutive_command_error;


    //- the Lakeshore State (OPSTR et HTRST)
    Tango::DevState tango_state;
    //- the values one can find in OPSTR command's response
    enum OPSTR_STATUS {
      ALARM      = 1,
      OVLD       = 2,
      RAMP1_DONE = 4,
      RAMP2_DONE = 8,
      NDRV       = 16,
      ATUNE      = 32,
      CAL        = 64,
      COM        = 128
    };
    enum RDGST_STATUS {
      RDGST_OK         = 0,
      INVALID_READING  = 1, 
      TEMP_UNDR        = 16,
      TEMP_OVR         = 32,
      SENSOR_UNITS_0   = 64,
      SENSOR_UNITS_OVR = 128
    };

    //- the value returned by OPSTR
    int opstr_state;
    //- the value returned by HTRST
    int htrst_state;
    //- the value returned by tRDGST
    int rdgst_state;

    std::string status_str;

    //- the temperature values read on the hard 
    double temperature_read [4];

    //- setpoints loop 1 and 2
    double setpoint_read;

    //- setpoints loop 1 and 2
    double output_read;

    //- OUTMODE the input number associated to the loop (the output number)
    //- [1...4]
    size_t input_number;
    //- OUTMODE the mode associated to the loop 
    //- (0=Off 1=Closed Loop 2=Zone 3=Open Loop 4=Monitor Out 5=Warmup)
    size_t mode;
    //- OUTMODE the power up enable associated to the loop (the output number)
    //- Powerup Enable : 0=Off 1=On
    size_t power_up_enable;

    size_t range;
    
    //- the PID settings
//    std::vector<PIDSettings> pid_settings;

    //- The regulation (Ramp) State Manager
    rsm_ns::RampingStateMachine * reg_manager;
    
  std::vector <std::string> com_state_enum_str; 


  };

}//- namespace
#endif //- __HW_PROXY_H__
