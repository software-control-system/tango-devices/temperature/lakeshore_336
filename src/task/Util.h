#ifndef _LAKESHORE_336_UTIL_H_
#define _LAKESHORE_336_UTIL_H_

//- no data : use example : 	post_msg(*this, MY_USER_MSG_WITHOUT_DATA, COMMAND_TIMEOUT_MS, false);
//- with data use example : 	post_msg(*this, MY_USER_MSG_WITH_DATA, COMMAND_TIMEOUT_MS, true, c );


#include <yat4tango/DeviceTask.h>
#include <yat4tango/CommonHeader.h>

namespace lakeshore_336_ns
{

void post_msg( yat4tango::DeviceTask &t, size_t msg_id, size_t timeout_ms, bool wait );

template <typename T>
void post_msg(yat4tango::DeviceTask &t, size_t msg_id, size_t timeout_ms, bool wait,
		T const &data )
{
	yat::Message *msg = NULL;
	try
	{
		msg = yat::Message::allocate(msg_id, DEFAULT_MSG_PRIORITY, wait);
	}
	catch( yat::Exception& ex )
	{
		RETHROW_YAT_ERROR(ex,
				"OUT_OF_MEMORY",
				"Out of memory",
				"post_msg");
	}

	try
	{
		msg->attach_data( data );
	}
	catch( yat::Exception& ex )
	{
		RETHROW_YAT_ERROR(ex,
				"SOFTWARE_FAILURE",
				"Unable to attach data",
				"post_msg");
	}

	try
	{
		if (wait)
			t.wait_msg_handled( msg, timeout_ms );
		else
			t.post( msg, timeout_ms );
	}
	catch( yat::Exception& ex )
	{
		RETHROW_YAT_ERROR(ex,
				"SOFTWARE_FAILURE",
				"Unable to post a msg",
				"ScanTask::post_msg");
	}
}
} //- namespace
#endif //- _LAKESHORE_336_UTIL_H_
