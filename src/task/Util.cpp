#include "./Util.h"

namespace lakeshore_336_ns
{


void post_msg( yat4tango::DeviceTask & t, size_t msg_id, size_t timeout_ms, bool wait )
{
	yat::Message* msg = 0;
	try
	{
		msg = yat::Message::allocate(msg_id, DEFAULT_MSG_PRIORITY, wait);
	}
	catch( yat::Exception& ex )
	{
		RETHROW_YAT_ERROR(ex,
				"OUT_OF_MEMORY",
				"Out of memory",
				"Utils::post_msg");
	}

	try
	{
		if (wait)
			t.wait_msg_handled( msg, timeout_ms );
		else
			t.post( msg, timeout_ms );
	}
	catch( yat::Exception& ex )
	{
		RETHROW_YAT_ERROR(ex,
				"SOFTWARE_FAILURE",
				"Unable to post a msg",
				"Utils::post_msg");
	}
}

} //- namespace
