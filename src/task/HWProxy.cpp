
//- Project : BT500
//- file : HWProxy.cpp

#include <task/HWProxy.h>
#include <math.h>
#include "StringTokenizer.h"
#include "task/Util.h"
#include <iomanip>


namespace HWProxy_ns
{
  // ============================================================================
  // Some defines and constants
  // ============================================================================
  static const double __NAN__ = ::sqrt(-1.);


  // ============================================================================
  // Config::Config
  // ============================================================================
  HWProxy::Config::Config ()
  {
    com_device_proxy    = "Not Initialised";
    startup_timeout_ms  = 5000;
    read_timeout_ms     = 1000;
    periodic_timeout_ms = 1000;
    time_in_deadband    = 60;
    loop_number = 1;
  }

  HWProxy::Config::Config (const Config & _src)
  {
    *this = _src;
  }

  // Config::operator =
  void HWProxy::Config::operator = (const Config & _src)
  {
    com_device_proxy    = _src.com_device_proxy;
    startup_timeout_ms  = _src.startup_timeout_ms;
    read_timeout_ms     = _src.read_timeout_ms;
    periodic_timeout_ms = _src.periodic_timeout_ms;
    time_in_deadband    = _src.time_in_deadband;
    if (_src.loop_number > 0 && _src.loop_number < 3)
      loop_number         = _src.loop_number;
    else
      loop_number = 0;
  }





  //-----------------------------------------------
  //- Ctor ----------------------------------------
  HWProxy::HWProxy (Tango::DeviceImpl * _host_device, 
                    Config & _conf) : 
              yat4tango::DeviceTask(_host_device),
              conf (_conf),
              host_dev (_host_device)
  {
    DEBUG_STREAM << "HWProxy::HWProxy <- " << std::endl;
    this->com_state = HWP_NO_ERROR;

    //- yat::Task configure optional msg handling
    this->enable_timeout_msg(true);
    this->enable_periodic_msg(true);
    this->set_periodic_msg_period(this->conf.periodic_timeout_ms);
    this->comproxy = 0;
    

    this->command_ok = 0;
    this->command_error = 0;
    this->consecutive_command_error = 0;
    
    com_state_enum_str.push_back (std::string ("HWP_UNKNOWN_ERROR"));
    com_state_enum_str.push_back (std::string ("HWP_INITIALIZATION_ERROR"));
    com_state_enum_str.push_back (std::string ("HWP_COMMUNICATION_ERROR"));
    com_state_enum_str.push_back (std::string ("HWP_COMMAND_ERROR"));
    com_state_enum_str.push_back (std::string ("HWP_HARDWARE_ERROR"));
    com_state_enum_str.push_back (std::string ("HWP_SOFTWARE_ERROR"));
    com_state_enum_str.push_back (std::string ("HWP_NO_ERROR"));

    //- check that Loop Number is correct
    if (conf.loop_number != 1 && conf.loop_number != 2)
    {
      ERROR_STREAM << "USER ERROR Loop Number <" << conf.loop_number << "> must be [1|2]" << std::endl;
      this->com_state = HWP_INITIALIZATION_ERROR;
      this->last_error = "USER ERROR Loop Number must be [1|2]\n";
    }

    //- create the Device Proxy
    try
    {
      DEBUG_STREAM << "HWProxy::HWProxy try to get proxy on " << conf.com_device_proxy << std::endl;
      this->comproxy = new yat4tango::ThreadSafeDeviceProxy (conf.com_device_proxy);
      if (this->comproxy == 0)
        throw std::bad_alloc ();
    }
    catch (const std::bad_alloc&)
    {
      ERROR_STREAM << "COM_ERROR could not allocate device proxy, memory error" << std::endl;
      this->com_state = HWP_INITIALIZATION_ERROR;
      this->last_error = "COM_ERROR could not allocate device proxy, memory error\n";
		  this->comproxy = 0;
    }
	  catch (Tango::DevFailed &e)
	  {
		  this->comproxy = 0;
      ERROR_STREAM << "could not connect to " << conf.com_device_proxy << " server DevFailed caught [" << e.errors[0].desc << "]" << std::endl;
      this->com_state = HWP_INITIALIZATION_ERROR;
      this->last_error = std::string ("could not connect to ") + 
                         conf.com_device_proxy + 
                         std::string (" server DevFailed caught [") + 
                         std::string (e.errors[0].desc) + 
                         std::string ("]\n");
	  }
    catch (...)
    {
		  this->comproxy = 0;
      ERROR_STREAM << "could not connect to " << conf.com_device_proxy << " server (...) caught" << std::endl;
      this->last_error = std::string ("could not connect to ") + 
                         conf.com_device_proxy  + 
                         std::string (" (...) caught\n");
      this->com_state = HWP_INITIALIZATION_ERROR;
    }

    //- RampingStateManager for state management (scan functionalities)
    try
    {
      DEBUG_STREAM << "HWProxy::HWProxy trying to allocate Regulation Manager" << std::endl;
      reg_manager = new rsm_ns::RampingStateMachine (host_dev, conf.time_in_deadband, 1);

    }
    catch (...)
    {
      ERROR_STREAM << "HWProxy::HWProxy Error trying to allocate Ramp State Manager" << std::endl; 
      this->com_state = HWP_INITIALIZATION_ERROR;
      this->last_error = "Error trying to allocate Ramp State Manager\n"; 
    }

    //- initialize the variables
    temperature_read [0] = temperature_read [1] = temperature_read [2] = temperature_read [3] = __NAN__;
    this->setpoint_read = __NAN__;
    this->input_number = 0;
    this->range = 0;
    this->mode = 0;
    this->range = 0;
    this->power_up_enable = 0;
    this->opstr_state = 0;
    this->htrst_state = 0;
    this->rdgst_state = 0;
  }


  //-----------------------------------------------
  //- Dtor ----------------------------------------
  HWProxy::~HWProxy (void)
  {
    DEBUG_STREAM << "HWProxy::~HWProxy <- " << std::endl;

    if (this->comproxy)
    {
      delete this->comproxy;
      this->comproxy = 0;
    }

    this->last_error = "No Error\n";
    if (reg_manager)
    {
      delete reg_manager;
      reg_manager = 0;
    }
  }



  //-----------------------------------------------
  //- the user core of the Task -------------------
  void HWProxy::process_message (yat::Message& _msg) throw (Tango::DevFailed)
  {
    //- The DeviceTask's lock_ -------------

    DEBUG_STREAM << "HWProxy::handle_message::receiving msg " << _msg.to_string() << std::endl;

    //- handle msg
    switch (_msg.type())
    {
      //- THREAD_INIT =======================
    case yat::TASK_INIT:
      {
        DEBUG_STREAM << "HWProxy::handle_message::TASK_INIT::thread is starting up" << std::endl;
        //- "initialization" code goes here
        last_error = "No Error\n";

        if (this->com_state == HWP_INITIALIZATION_ERROR)
        {
          ERROR_STREAM << "HWProxy::handle_message::TASK_INIT DO NOTHING REASON : INITIALIZATION_ERROR" << std::endl;
          INFO_STREAM << "HWProxy::handle_message::TASK_INIT::com_state = " << int (com_state) << " str = " << com_state_enum_str [com_state] << com_state<< std::endl;
          return;
        }

        //- initialize variables by reading HW
       DEBUG_STREAM << "HWProxy::handle_message::TASK_INIT::initializing variables" << std::endl;
         
        try
        {
          this->read_temperatures ();
          this->read_setpoint ();
          this->read_outmode ();
          this->read_range ();
        }
        catch (...)
        {
          this->com_state = HWP_COMMUNICATION_ERROR;
          this->last_error = "Error TASK_INIT Error trying to read HW parameters [try to restart devicce]\n"; 
          ERROR_STREAM << "HWProxy::process_message::TASK_INIT Error trying read HW parameters" << std::endl; 
        }

      } 
      break;
      //- TASK_EXIT =======================
    case yat::TASK_EXIT:
      {
        DEBUG_STREAM << "HWProxy::handle_message handling TASK_EXIT thread is quitting" << std::endl;

        //- "release" code goes here
      }
      break;
      //- TASK_PERIODIC ===================
    case yat::TASK_PERIODIC:
      {
        DEBUG_STREAM << "HWProxy::handle_message handling TASK_PERIODIC msg " << std::endl;
        //- code relative to the task's periodic job goes here
        if (com_state == HWP_INITIALIZATION_ERROR)
        {
          ERROR_STREAM << "HWProxy::handle_message::TASK_PERIODIC DO NOTHING REASON : INITIALIZATION_ERROR" << std::endl;
          return;
        }

        //- get the 4 temperatures KRDG
        this->read_temperatures ();

        //- update the temperature for the state manager for scan
        reg_manager->update_temperature (temperature_read [this->input_number -1]);

        //- get the status RDGST HTRST OPSTR
        this->read_lakeshore_state ();

        //- get the output value
        this->read_output ();

        //- get the current setpoint
        this->read_setpoint ();
     }
      break;

      //- TASK_TIMEOUT ===================
    case yat::TASK_TIMEOUT:
      {
        //- code relative to the task's tmo handling goes here
        ERROR_STREAM << "HWProxy::handle_message handling TASK_TIMEOUT msg" << std::endl;
        this->com_state = HWP_COMMUNICATION_ERROR;
        this->last_error = "HWProxy::handle_message received TASK_TIMEOUT msg\n";
      }
      break;

      //- USER_DEFINED_MSG ================
    //- low level commands (ASCII commands directly passed to the Lakeshore 336 (appending LF at the end)
    case EXEC_LOW_LEVEL_MSG:
      {
        DEBUG_STREAM << "HWProxy::handle_message handling EXEC_LOW_LEVEL_MSG msg" << std::endl;
        LowLevelMsg * llm = 0;
        _msg.detach_data(llm);
        if (llm)
        {
          this->exec_low_level_command (llm->cmd);
          delete llm;
        }
      }
      break;

      case SET_DEADBAND_MSG:
        {
          DEBUG_STREAM << "HWProxy::handle_message handling SET_DEADBAND_MSG msg" << std::endl;
          double * double_value = 0;
          //- bool wr_ok = false;
          _msg.detach_data(double_value);
          if (double_value) 
          {
            reg_manager->set_deadband (*double_value);
          }
        }
        break;

    //- the way to set SETPOINT for loop [1|2]
    case SET_SETPOINT_MSG:
      {
        DEBUG_STREAM << "HWProxy::handle_message handling SET_SETPOINT_MSG msg" << std::endl;
        double * val = 0;
        _msg.detach_data(val);
        if(val)
        {
          std::stringstream s;
          s << "SETP " 
            << this->conf.loop_number << ","
            << std::fixed
            << std::setprecision (2)
            << *val 
            << std::endl;
          this->write (s.str ());

          this->reg_manager->update_setpoint (*val);

          delete val;
        }
      }
      break;
    case STOP_MSG:
      {
        DEBUG_STREAM << "HWProxy::handle_message handling STOP_MSG msg" << std::endl;

        //- force the setpoints loop 1 and loop 2 to the current value
        yat::Message * msg = 0;
        msg = new yat::Message(HWProxy_ns::SET_SETPOINT_MSG);
        if (msg == 0)
        {
          ERROR_STREAM << "HWProxy::Stop error trying to create msg "<< endl;
          Tango::Except::throw_exception (_CPTC ("OUT_OF_MEMORY"),
                                          _CPTC ("yat::Message allocation failed"),
                                          _CPTC ("HWProxy::process_message::STOP_MSG"));
        }
        //- to stop write setpoint to the temperature value
        double val = temperature_read [input_number -1];
        msg->attach_data (val);
        this->post(msg);

        //- stop the state manager for scans
        this->reg_manager->stop ();
      }
      break;

    case LOOP_SELECT_INPUT_MSG :
      {
        DEBUG_STREAM << "HWProxy::handle_message handling LOOP_SELECT_INPUT_MSG msg" << std::endl;
        size_t * val = 0;
        _msg.detach_data(val);
        if(val)
        {
          if (*val < 0 || *val > 5)
          {
            ERROR_STREAM << "HWProxy::process_messsage::LOOP_SELECT_INPUT_MSG OUT_OF_RANGE input must be in [1...4] "<< endl;
            Tango::Except::throw_exception (_CPTC ("OUT_OF_RANGE"),
                                            _CPTC ("input must be in [1...4] "),
                                            _CPTC ("HWProxy::process_messsage::LOOP_SELECT_INPUT_MSG"));
          }

          std::stringstream s;
          s << "OUTMODE " 
            << this->conf.loop_number << ","
            << this->mode << ","
            << *val << ","
            << this->power_up_enable
            << std::endl;
          this->write (s.str ());
        }
        if (val)
          delete val;

        //- update the internal variables by reading HW
        this->read_outmode ();
      }
      break;

    case LOOP_SELECT_RANGE_MSG :
      {
        DEBUG_STREAM << "HWProxy::handle_message handling LOOP_SELECT_RANGE_MSG msg" << std::endl;
        size_t * val = 0;
        _msg.detach_data(val);
        if(val)
        {
          if (*val < 0 || *val > 3)
          {
            ERROR_STREAM << "HWProxy::process_messsage::LOOP_SELECT_RANGE_MSG OUT_OF_RANGE input must be in [0...3] "<< endl;
            Tango::Except::throw_exception (_CPTC ("OUT_OF_RANGE"),
                                            _CPTC ("input must be in [0...3] "),
                                            _CPTC ("HWProxy::process_messsage::LOOP_SELECT_RANGE_MSG"));
          }

          std::stringstream s;
          s << "RANGE " 
            << this->conf.loop_number << ","
            << *val 
            << std::endl;
          this->write (s.str ());
        }
        if (val)
          delete val;
        //- update the internal variables by reading HW
        this->read_range ();
      }
      break;

    default:
  		  ERROR_STREAM<< "HWProxy::handle_message::unhandled msg type received" << std::endl;
  		break;
    } //- switch (_msg.type())
  } //- HWProxy::process_message




  //-----------------------------------------------
  //- read_temperatures () read temperatures on the hard
  //- gets the temperature and on the hard
  //-----------------------------------------------
  bool HWProxy::read_temperatures ()
  {
    DEBUG_STREAM << "HWProxy::read_temperatures <-" << std::endl;
    //- case of  device proxy error
    if (com_state == HWP_INITIALIZATION_ERROR)
      return false;
    static std::string cmd [] = {
      "KRDG? A\n",
      "KRDG? B\n",
      "KRDG? C\n",
      "KRDG? D\n"};

    std::string response;

    //- lire la temperature
    for (size_t i = 0; i < 4; i++)
    {
      if (! this->write_read (cmd [i], response))
      {
        ERROR_STREAM << "HWProxy::read_temperatures () error reading temperature" << std::endl;
        this->com_state = HWP_COMMAND_ERROR;
        this->command_error ++;
        return false;
      }
      else
      {
        //- parse the response and put it in the array
        //- response form 
        StringTokenizer tok (",", response);
        try 
        {
          this->temperature_read [i] = tok.get_token <double> (0);
          DEBUG_STREAM << "HWProxy::read_temperatures temperature " << i << " = " << this->temperature_read [i] << std::endl;
        }
        catch (...)
        {
        FATAL_STREAM << "HWProxy::read_temperature parsing error cmd [" << cmd [i] << "] response [" << response << std::endl;
        this->com_state = HWP_COMMAND_ERROR;
        this->last_error = std::string ("parsing error cmd [") + cmd [i] + std::string ("] response [") + response + "]\n";
        return false;
        }
      }
    }
    this->command_ok ++;
    return true;

  }
 
  //-----------------------------------------------
  //- read_setpoint () read setpoint on the hard
  //-----------------------------------------------
  bool HWProxy::read_setpoint ()
  {
    DEBUG_STREAM << "HWProxy::read_setpoint <-" << std::endl;
    //- case of  device proxy error
    if (com_state == HWP_INITIALIZATION_ERROR)
      return false;


    std::stringstream cmd; 
    cmd << "SETP? "
        << conf.loop_number
        << std::endl;

    std::string response;

    //- lire la consigne
    if (! this->write_read (cmd.str (), response))
    {
      ERROR_STREAM << "HWProxy::read_setpoint () error reading setpoint" << std::endl;
      this->com_state = HWP_COMMAND_ERROR;
      this->command_error++;
      return false;
    }
    else
    {
      //- parse the response and put it in the array
      //- response form 
        StringTokenizer tok (",", response);
      try 
      {
        this->setpoint_read = tok.get_token <double> (0);
        DEBUG_STREAM << "HWProxy::read_setpoint value = " << this->setpoint_read << std::endl;
      }
      catch (...)
      {
        FATAL_STREAM << "HWProxy::read_setpoint parsing error cmd [" << cmd.str () << "] response [" << response << std::endl;
        this->last_error = std::string ("parsing error cmd [") + cmd.str () + std::string ("] response [") + response + "]\n";
        this->com_state = HWP_COMMAND_ERROR;
        return false;
      }
    }
    this->command_ok ++;
    return true;
  } 

  //-----------------------------------------------
  //- read_output () read output percent on the hard
  //-----------------------------------------------
  bool HWProxy::read_output ()
  {
    DEBUG_STREAM << "HWProxy::read_output <-" << std::endl;
    //- case of  device proxy error
    if (com_state == HWP_INITIALIZATION_ERROR)
      return false;


    std::stringstream cmd; 
    cmd << "HTR? "
        << conf.loop_number
        << std::endl;

    std::string response;

    //- lire la consigne
    if (! this->write_read (cmd.str (), response))
    {
      ERROR_STREAM << "HWProxy::read_output () error reading output" << std::endl;
      this->com_state = HWP_COMMAND_ERROR;
      this->command_error++;
      return false;
    }
    else
    {
      //- parse the response and put it in the array
      //- response form 
        StringTokenizer tok (",", response);
      try 
      {
        this->output_read = tok.get_token <double> (0);
        DEBUG_STREAM << "HWProxy::read_output value = " << this->output_read << std::endl;
      }
      catch (...)
      {
        FATAL_STREAM << "HWProxy::read_output parsing error cmd [" << cmd.str () << "] response [" << response << std::endl;
        this->last_error = std::string ("parsing error cmd [") + cmd.str () + std::string ("] response [") + response + "]\n";
        this->com_state = HWP_COMMAND_ERROR;
        return false;
      }
    }
    this->command_ok ++;
    return true;
  } 

  //-----------------------------------------------
  //- read_outmode () read out mode on the hard
  //-----------------------------------------------
  bool HWProxy::read_outmode ()
  {
    DEBUG_STREAM << "HWProxy::read_outmode <-" << std::endl;
    //- case of  device proxy error
    if (com_state == HWP_INITIALIZATION_ERROR)
      return false;
     std::stringstream cmd;
     cmd << "OUTMODE? "
         << conf.loop_number
         << std::endl;

    std::string response;

    //- lire la reponse
    if (! this->write_read (cmd.str (), response))
    {
      ERROR_STREAM << "HWProxy::read_outmode () error reading OUTMODE" << std::endl;
      this->com_state = HWP_COMMAND_ERROR;
      this->command_error++;
      return false;
    }
    else
    {
      //- parse the response and put it in the array
      //- response form 
      StringTokenizer tok (",", response);
      try 
      {
        this->mode = tok.get_token <size_t> (0);
        this->input_number = tok.get_token <size_t> (1);
        this->power_up_enable = tok.get_token <size_t> (2);
        INFO_STREAM << "HWProxy::read_setpoints read_outmode loop "
                     << conf.loop_number
                     << " mode <" << this->mode << ">"
                     << " input <" << this->input_number << ">"
                     << " powerup enable <" << this->power_up_enable << ">"
                     << std::endl;
      }
      catch (...)
      {
      FATAL_STREAM << "HWProxy::read_outmode parsing error cmd [" << cmd.str () << "] response [" << response << std::endl;
      this->last_error = std::string ("parsing error cmd [") + cmd.str () + std::string ("] response [") + response + "]\n";
      this->com_state = HWP_COMMAND_ERROR;
      return false;
      }
    }
    this->com_state = HWP_NO_ERROR;

    this->command_ok ++;
    return true;
  }

  //-----------------------------------------------
  //- read_range () read range on the hard
  //-----------------------------------------------
  bool HWProxy::read_range ()
  {
    DEBUG_STREAM << "HWProxy::read_range <-" << std::endl;
    //- case of  device proxy error
    if (com_state == HWP_INITIALIZATION_ERROR)
      return false;
     std::stringstream cmd;
     cmd << "RANGE? "
         << conf.loop_number
         << std::endl;

    std::string response;

    //- lire la reponse
    if (! this->write_read (cmd.str (), response))
    {
      ERROR_STREAM << "HWProxy::read_range () error reading RANGE" << std::endl;
      this->com_state = HWP_COMMAND_ERROR;
      this->command_error++;
      return false;
    }
    else
    {
      //- parse the response and put it in the array
      //- response form 
      StringTokenizer tok (",", response);
      try 
      {
        this->range = tok.get_token <size_t> (0);
        INFO_STREAM << "HWProxy::read_range loop "
                     << conf.loop_number
                     << " range <" << this->range << ">"
                      << std::endl;
      }
      catch (...)
      {
      FATAL_STREAM << "HWProxy::read_range parsing error cmd [" << cmd.str () << "] response [" << response << std::endl;
      this->com_state = HWP_COMMAND_ERROR;
      this->last_error = std::string ("parsing error cmd [") + cmd.str () + std::string ("] response [") + response + "]\n";
      return false;
      }
    }
    this->com_state = HWP_NO_ERROR;

    this->command_ok ++;
    return true;
  }

  //-----------------------------------------------
  //- read_lakeshore_state
  //- get the Lakehore 336 state
  //-----------------------------------------------
  bool HWProxy::read_lakeshore_state ()
  {
    DEBUG_STREAM << "HWProxy::read_lakeshore_state <-" << std::endl;
    //- case of  device proxy error
    if (com_state == HWP_INITIALIZATION_ERROR)
      return false;

    std::string response;

     std::string cmd = "OPSTR?\n";
    if (this->write_read (cmd, response))
    {
      StringTokenizer tok(",", response);
      if (tok.get_number_of_token () < 1)
      {
        this->com_state = HWP_COMMAND_ERROR;
        FATAL_STREAM << "WProxy::read_lakeshore_state invalid command [" << cmd << "] response [" << response << "]" << std::endl;
        this->last_error = "WProxy::read_lakeshore_state invalid command [" + cmd + "] response [" + response + "]\n";
      }
      try
      {
        this->opstr_state = tok.get_token <int> (0);
      }
      catch (...)
      {
        FATAL_STREAM << "HWProxy::read_lakeshore_state parsing command error cmd [" << cmd << "] response [" << response << std::endl;
        this->last_error = "HWProxy::read_lakeshore_state parsing command error cmd [" + cmd + "] response [" + response;
        return false;
      }
    }

    {
      std::stringstream s;
      s << "HTRST? " << this->conf.loop_number << std::endl;
  //-     cmd = "HTRST? \n";
      if (this->write_read (s.str (), response))
      {
        StringTokenizer tok(",", response);
        if (tok.get_number_of_token () < 1)
        {
          this->com_state = HWP_COMMAND_ERROR;
          this->last_error = "WProxy::read_lakeshore_state invalid command [" + cmd + "] response [" + response + "]\n";
        }
        try
        {
          this->htrst_state = tok.get_token <int> (0);
        }
        catch (...)
        {
          FATAL_STREAM << "HWProxy::read_lakeshore_state parsing command error cmd [" << cmd << "] response [" << response << std::endl;
          this->last_error = "HWProxy::read_lakeshore_state (...) exception parsing command error cmd [" + cmd + "] response [" + response;
          return false;
        }
        return true;
      }
      else
      {
        com_error ++;
        FATAL_STREAM << "HWProxy::read_lakeshore_state response error to cmd [" << cmd << "] response [" << response << std::endl;
        this->status_str = "HWProxy::read_lakeshore_state response error to cmd [" + cmd + "] response [" + response;
        return false;
      }
    }

    std::stringstream s;
    s << "RDGST? " << this->input_number << std::endl;
    if (this->write_read (s.str (), response))
    {
      StringTokenizer tok(",", response);
      if (tok.get_number_of_token () < 1)
      {
        this->com_state = HWP_COMMAND_ERROR;
        this->last_error = "WProxy::read_lakeshore_state invalid command [" + cmd + "] response [" + response + "]\n";
      }
      try
      {
        this->htrst_state = tok.get_token <int> (0);
      }
      catch (...)
      {
        FATAL_STREAM << "HWProxy::read_lakeshore_state parsing command error cmd [" << cmd << "] response [" << response << std::endl;
        this->last_error = "HWProxy::read_lakeshore_state (...) exception parsing command error cmd [" + cmd + "] response [" + response;
        return false;
      }
      return true;
    }
    else
    {
      com_error ++;
      FATAL_STREAM << "HWProxy::read_lakeshore_state response error to cmd [" << cmd << "] response [" << response << std::endl;
      this->status_str = "HWProxy::read_lakeshore_state response error to cmd [" + cmd + "] response [" + response;
      return false;
    }
    return true;
  }
  
  //-----------------------------------------------
  //- returns the lakeshore state machine value;
  //- 
  //-----------------------------------------------
  Tango::DevState HWProxy::get_lakeshore_state (void)
  {
  
    DEBUG_STREAM << "HWProxy::get_lakeshore_state <-" << std::endl;
    
   //- case of  device proxy error
    if (com_state == HWP_INITIALIZATION_ERROR)
    {
      tango_state = Tango::FAULT;
      return tango_state;
    }


    //- HTRST heater open circuit or short circuit
    if (this-> htrst_state != 0)
    {
      tango_state = Tango::FAULT;
      return tango_state;
    }

    //- RDGST return status
    //- Invalid Reading
    if ((this->rdgst_state & INVALID_READING) != 0)
    {
      tango_state = Tango::FAULT;
      return tango_state;
    }
    //- Temp Under Range | Temp OverRange | Sensor Units 0 | Sensor Units OverRange
    if (((this->rdgst_state & TEMP_UNDR)        != 0) ||
        ((this->rdgst_state & TEMP_OVR)         != 0) ||
        ((this->rdgst_state & SENSOR_UNITS_0)   != 0) ||
        ((this->rdgst_state & SENSOR_UNITS_OVR) != 0))

    {
      tango_state = Tango::ALARM;
      return tango_state;
    }


    //- OPSTR return status
    //- bit 0 Alarm | bit 1 Overload
    if (((this->opstr_state & ALARM) != 0) ||
        ((this->opstr_state & OVLD)  != 0))
    {
      tango_state = Tango::ALARM;
      return tango_state;
    }
    //- bit 5 Autotune Error | bit 6 Calibration Error | bit 7 Processor Com Error
     else if (((this->opstr_state & ATUNE) != 0) || 
              ((this->opstr_state & CAL)   != 0) || 
              ((this->opstr_state & COM)   != 0) || 
               (htrst_state != 0))
    {
      tango_state = Tango::FAULT;
      return tango_state;
    }
    else
    {
      //- normal operation conditions
      tango_state = reg_manager->get_state ();
      return tango_state;
    }
      
    //- finally if no match found
    return Tango::UNKNOWN;   
   }
   
  //-----------------------------------------------
  //- returns the Lakeshore 336 status machine value;
  //- 
  //-----------------------------------------------
  std::string HWProxy::get_lakeshore_status (void)
  {
    DEBUG_STREAM << "HWProxy::get_lakeshore_status <-" << std::endl;
    
    //- case of  device proxy error
    if (com_state == HWP_INITIALIZATION_ERROR)
      return "Initialisation error";
    status_str.clear ();

    std::stringstream s;
    s << "Loop Number <" << conf.loop_number << "> uses Input <" << this->input_number << "> as process variable" << std::endl;
    status_str = s.str ();
      
    //- Operation Status Register
    if (this->opstr_state != 0)
      status_str += "OPSTR Status : \n";
    if ((this->opstr_state & ALARM) != 0)
      status_str += "Lakeshore in ALARM\n";
    if ((this->opstr_state & OVLD) != 0)
      status_str += "Lakeshore Overload\n";
    if ((this->opstr_state & RAMP1_DONE) != 0)
      status_str += "Ramp 1 Done\n";
    if ((this->opstr_state & RAMP2_DONE) != 0)
      status_str += "Ramp 2 Done\n";
    if ((this->opstr_state & NDRV) != 0)
      status_str += "New Sensor Reading\n";
    if ((this->opstr_state & ATUNE) != 0)
      status_str += "Autotune Error\n";
    if ((this->opstr_state & CAL) != 0)
      status_str += "Caibration Error\n";
    if ((this->opstr_state & CAL) != 0)
      status_str += "Processor Com Error\n";

    //- Heater Status 
    status_str += "Heater Status :\n";
    if (this->htrst_state == 0)
    status_str += "No error\n";
    else if (this->htrst_state == 1)
    status_str += "Heater Open Load\n";
    else if (this->htrst_state == 2)
    status_str += "Heater Short Circuit\n";

     //- Input Reading Status
    status_str += "Input Status RDGST :\n";
    if ((this->rdgst_state & INVALID_READING) != 0)
      status_str += "Invalid Reading\n";
    if ((this->rdgst_state & TEMP_UNDR) != 0)
      status_str += "Temperature Under Range\n";
    if ((this->rdgst_state & TEMP_OVR) != 0)
      status_str += "Temperature Over Range\n";
    if ((this->rdgst_state & SENSOR_UNITS_0) != 0)
      status_str += "Sensor Units Zero\n";
    if ((this->rdgst_state & SENSOR_UNITS_OVR) != 0)
      status_str += "Sensor Units Over Range\n";

    //- ramp state 
    status_str += reg_manager->get_status ();

    return status_str;
   }


  //-----------------------------------------------
  //- write_read
  //- send command returns result
  //-----------------------------------------------
  bool HWProxy::write_read (std::string cmd, std::string & response)
  {
    DEBUG_STREAM << "HWProxy::write_read <-" << std::endl;
    //- case of  device proxy error
    if (com_state == HWP_INITIALIZATION_ERROR || this->comproxy == 0)
      return false;

    Tango::DeviceData dd_in;
    Tango::DeviceData dd_out;

    try
    {
      { //- CRITICAL SECTION
        yat::AutoMutex<> guard(this->lock);

        //- make sure response is empty
        response.clear ();

        DEBUG_STREAM << "HWProxy::write_read try to send command [" << cmd << "]" << std::endl;
        Tango::DevString ds;
        ds = CORBA::string_dup (cmd.c_str ());
        dd_in << ds;
        dd_out = this->comproxy->command_inout("WriteRead", dd_in);
        dd_out >> response;
        DEBUG_STREAM << "HWProxy::write_read response to command [" << cmd << "] received [" << response << "]" << std::endl;
      }
      //- response empty
      if (response.size () <= 0)
      {
        this->com_state = HWP_COMMUNICATION_ERROR;
        this->last_error = "empty response to cmd [" + cmd + "]\n";
        this->command_error ++;
        return false;
      }
    }
    catch (Tango::DevFailed &e)
    {
      ERROR_STREAM << "HWProxy::write_read()caugth DevFailed [" << e << "]" << std::endl;
      this->com_state = HWP_COMMUNICATION_ERROR;
      this->last_error = "communication failed for command Tango Exception caught" + cmd;
      this->com_error ++;
      return false;
    } 
    catch (...)
    {
      ERROR_STREAM << "HWProxy::write_read() caught (...) " << std::endl;
      this->com_state = HWP_COMMUNICATION_ERROR;
      this->last_error = "communication failed for command (...) caught" + cmd;
      this->com_error ++;
      return false;
    }
    this->com_state = HWP_NO_ERROR;
    this->command_ok ++;
    this->com_success ++;

    return true;
  }

  //-----------------------------------------------
  //- write
  //- send a command  (no response expected)
  //-----------------------------------------------
  bool HWProxy::write (std::string cmd)
  {
    DEBUG_STREAM << "HWProxy::write <-" << std::endl;
    //- case of  device proxy error
    if (com_state == HWP_INITIALIZATION_ERROR || this->comproxy == 0)
      return false;

    Tango::DeviceData dd_in;

    try
    {
      { //- CRITICAL SECTION
        yat::AutoMutex<> guard(this->lock);

        DEBUG_STREAM << "HWProxy::write try to send command [" << cmd << "]" << std::endl;
        Tango::DevString ds;
        ds = CORBA::string_dup (cmd.c_str ());
        dd_in << ds;
        this->comproxy->command_inout("Write", dd_in);
      }
    }
    catch (Tango::DevFailed &e)
    {
      ERROR_STREAM << "HWProxy::write caugth DevFailed [" << e << "]" << std::endl;
      this->com_state = HWP_COMMUNICATION_ERROR;
      this->last_error = std::string ("communication failed for command [") + 
                         cmd + 
                         std::string ("] Tango Exception caught\n");
      this->com_error ++;
      return false;
    } 
    catch (...)
    {
      ERROR_STREAM << "HWProxy::write caught (...) " << std::endl;
      this->com_state = HWP_COMMUNICATION_ERROR;
      this->last_error = std::string ("communication failed for command [") + 
                         cmd + 
                         std::string ("] (...) caught\n");
      this->com_error ++;
      return false;
    }
    this->command_ok ++;
    this->com_success++;
    this->com_state = HWP_NO_ERROR;
    return true;
  }



  //-----------------------------------------------
  //- exec_low_level_command
  //- sends a Low Level Command (do not check the syntax)  to the hard -blocking!
  //-----------------------------------------------
  std::string HWProxy::exec_low_level_command (std::string cmd)
  {
    DEBUG_STREAM << "HWProxy::exec_low_level_command for command [" << cmd << "\n]" << std::endl;
    std::string response;
    cmd += "\n";
    //- this is a reading, response expected
    if (cmd.find ("?") != std::string::npos)
    {
      this->write_read (cmd, response);
      return response;
    }
    //- else it is a command, HW does not send a responsd, return just a " "
    else
    {
      this->write (cmd);
      return std::string(" ");
    }
  }


} //- namespace
